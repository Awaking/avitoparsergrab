#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from grab import Grab, GrabError
from grab.spider import Spider, Task
from datetime import datetime, date
from datetime import timedelta
import time
import locale
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, create_engine, desc
from sqlalchemy.types import DateTime, Boolean
from sqlalchemy.orm import sessionmaker
import logging
from weblib.error import  DataNotFound
import json
import re

locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8') # the ru locale is installed
base = declarative_base()


class AvitoXPathConstants:
    SEARCH_ADV_BLOCK = "//div[@class='item item_table clearfix js-catalog-item-enum c-b-0']"
    SEARCH_ADV_BLOCK_TITLE = "*/h3[@class='title item-description-title']/a[@class='item-description-title-link']"
    #SEARCH_ADV_BLOCK_PRICE = "*/div[@class='about']"
    #SEARCH_ADV_BLOCK_DATE = "*/div[@class='data']/div[@class='clearfix ']/div[@class='date c-2']"

    ADV_METADATA_BLOCK = "//div[@class='title-info-metadata']"
    ADV_METADATA_BLOCK_DATE = "//div[@class='title-info-metadata-item'][1]"
    ADV_METADATA_BLOCK_VIEWS = "//div[@class='title-info-metadata-item'][2]"
    ADV_METADATA_BLOCK_VIEWS_METADATA = "//a[@class='js-show-stat pseudo-link']"
    ADV_DESCRIPTION_TEXT = "//div[@class='item-description-text']"
    ADV_DESCRIPTION_HTML = "//div[@class='item-description-html']"
    ADV_PRICE = "//div[@class='item-price-value-wrapper']/div[@id='price-value']/span[@class='price-value-string']"
    ADV_TITLE = "//span[@class='title-info-title-text']"

    M_ADV_DESCRIPTION = "//div[@class='description-preview-wrapper']"
    M_ADV_METADATA_VIEWS = "//div[@class='item-view-count']"
    M_ADV_METADATA_DATE = "//div[@class='item-add-date']"
    M_ADV_PHONE_URL_REX = 'js-action-show-number  "\n href="(.+)"'


class City(base):
    __tablename__ = 'City'
    name = Column(String, primary_key=True)
    NOVOSIBIRSK = "novosibirsk"
    BELGOROD = "belgorod"

    def __init__(self, name):
        self.name = name


class Category(base):
    __tablename__ = 'Category'
    name = Column(String, primary_key=True)
    ANY = ""

    def __init__(self, name):
        self.name = name


class ViewsHistory(base):
    __tablename__ = 'ViewsHistory'
    date = Column(DateTime, primary_key=True)
    views_count = Column(Integer, nullable=False)
    fk_avito_adv_id = Column(Integer, ForeignKey("AvitoAdv.id"), primary_key=True)

    def __init__(self, date, views_count, fk_avito_adv_id):
        self.date = date
        self.views_count = views_count
        self.fk_avito_adv_id = fk_avito_adv_id

    def __str__(self):
        return "%s views at %s for id %s" % (self.views_count, self.date, self.fk_avito_adv_id)


class PriceHistory(base):
    __tablename__ = 'PriceHistory'
    date = Column(DateTime, primary_key=True)
    price = Column(Integer, nullable=False)
    fk_avito_adv_id = Column(Integer, ForeignKey("AvitoAdv.id"), primary_key=True)

    def __init__(self, date, price, fk_avito_adv_id):
        self.date = date
        self.price = price
        self.fk_avito_adv_id = fk_avito_adv_id

    def __str__(self):
        return "Price = %s at %s for id %s" % (self.price, self.date, self.fk_avito_adv_id)


class AvitoAdv(base):
    __tablename__ = 'AvitoAdv'
    id  = Column(Integer, primary_key=True)
    title = Column(String)
    url = Column(String)
    description = Column(String)
    placement_date = Column(DateTime)
    update_date = Column(DateTime)
    phone = Column(String)
    last_checked = Column(DateTime)
    is_closed = Column (Boolean)
    is_old = Column(Boolean)
    fk_city = Column(String, ForeignKey("City.name"))
    fk_category = Column(String, ForeignKey("Category.name"))

    price = None
    views_count = None
    item_parsed_callback = None

    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "ID: %s, Title: %s, URL: %s, Placement Date: %s, Update Date: %s, Last_Checked: %s,  Phone: %s," \
               "Closed: %s, Old: %s, City: %s, Category: %s, Description: %s " \
               % (self.id, self.title, self.url, self.placement_date, self.update_date, self.last_checked, self.phone,
                  self.is_closed, self.is_old, self.fk_city, self.fk_category, self.description)


class DBManager:

    def __init__(self, datebase="datebase.db", decl_base = None, is_debug = False):
        if not decl_base:
            decl_base = declarative_base()

        engine = create_engine('sqlite:///' + datebase, echo=is_debug, connect_args={'check_same_thread': False})
        decl_base.metadata.create_all(engine)
        SessionMaker = sessionmaker(bind=engine)
        self.session = SessionMaker()

    def city_with_name (self, name):
        name_lower = name.lower()
        city = self.session.query(City).filter_by(name=name_lower).first()
        if not city:
            city = City(name_lower)
            self.session.add(city)
        return city

    def category_with_name (self, name):
        name_lower = name.lower()
        category = self.session.query(Category).filter_by(name=name_lower).first()
        if not category:
            category = Category(name_lower)
            self.session.add(category)
        return category

    def avito_adv_by_id(self, avito_adv_id):
        obj = self.session.query(AvitoAdv).filter_by(id = avito_adv_id).first()
        return obj

    def views_history_for_avito_adv_id(self, avito_adv_id):
        return self.session.query(ViewsHistory).filter_by(fk_avito_adv_id = avito_adv_id, date = date.today()).first()

    def price_history_for_avito_adv_id(self, avito_adv_id):
        return self.session.query(PriceHistory).filter_by(fk_avito_adv_id = avito_adv_id, date = date.today()).first()

    def synchronize_avito_advs(self, avito_advs):
        for avito_adv in avito_advs:
            self.synchronize_avito_adv(avito_adv)

    def synchronize_avito_adv(self, avito_adv):

        stored_avito_adv = self.avito_adv_by_id(avito_adv.id)
        if stored_avito_adv:
            if avito_adv.title:
                stored_avito_adv.title = avito_adv.title
            if avito_adv.url:
                stored_avito_adv.url = avito_adv.url
            if avito_adv.description:
                stored_avito_adv.description = avito_adv.description
            if avito_adv.placement_date:
                stored_avito_adv.placement_date = avito_adv.placement_date
            if avito_adv.update_date:
                stored_avito_adv.update_date = avito_adv.update_date
            if avito_adv.phone:
                stored_avito_adv.phone = avito_adv.phone
            if avito_adv.is_closed:
                stored_avito_adv.is_closed = avito_adv.is_closed
            if avito_adv.is_old:
                stored_avito_adv.is_old = avito_adv.is_old
            if avito_adv.last_checked:
                stored_avito_adv.last_checked = avito_adv.last_checked
            self.session.merge(stored_avito_adv)
        else:
            self.session.merge(avito_adv)

        if avito_adv.views_count:
            views_history_record = ViewsHistory(date.today(), avito_adv.views_count,avito_adv.id)
            self.session.merge(views_history_record)

        if avito_adv.price:
            price_history_record = PriceHistory(date.today(), avito_adv.price, avito_adv.id)
            self.session.merge(price_history_record)

        if avito_adv.fk_city:
            avito_adv.fk_city = self.city_with_name(avito_adv.fk_city)

        if avito_adv.fk_category:
            avito_adv.fk_category = self.category_with_name(avito_adv.fk_category)

        self.save();


    def save(self):
        self.session.commit()

    def print_all_avito_advs(self):
        for instance in self.session.query(AvitoAdv):
            print(instance)
            last_views_count = self.last_adv_views_count(instance.id)
            if last_views_count:
                print ("..........", last_views_count)
            last_price = self.last_adv_price(instance.id)
            if last_price:
                print ("..........", last_price)

    def last_adv_views_count (self, avito_adv_id):
        obj = self.session.query(ViewsHistory).filter_by(fk_avito_adv_id=avito_adv_id).order_by(desc(ViewsHistory.date)).first()
        return obj

    def last_adv_price (self, avito_adv_id):
        obj = self.session.query(PriceHistory).filter_by(fk_avito_adv_id=avito_adv_id).order_by(desc(PriceHistory.date)).first()
        return obj


class TaskManager:

    @staticmethod
    def search_pages_urls(search_phrase, category = '', city = None, pages = [1]):
        assert city
        urls = []
        for page in pages:
            url = u'https://www.avito.ru/%s' % city.name
            if category:
                url = u'%s/%s' % (url, category)

            url = u'%s?p=%d' % (url, page)
            if search_phrase:
                url = u'%s&q=%s' % (url, search_phrase)
            urls.append(url)
        return urls


class AvitoSearchSpider(Spider):

    def prepare(self):
        self.items_urls = []

    def task_initial(self, grab, task):

        found_adv_blocks = grab.doc.select(AvitoXPathConstants.SEARCH_ADV_BLOCK)
        for adv_block in found_adv_blocks:
            title = adv_block.select(AvitoXPathConstants.SEARCH_ADV_BLOCK_TITLE)
            title_url = grab.make_url_absolute(title.attr('href'))
            self.items_urls.append(title_url)

    def task_initial_fallback(self, task):
        if self.args["search_errors_file"]:
            file_name = self.args["search_errors_file"]
        else :
            file_name = "search_errors.log"
        with open(file_name, 'a') as file:
            file.write("[%s] %s\n" %(datetime.now(), task.url))


class AvitoUpdateSpider(Spider):

    def prepare(self):
        self.advs_processed = 0

    def task_initial(self, grab, task):

        if self.args["is_debug"]:
            print("Updating item from URL: ", task.url, "...")

        id = AvitoUpdateSpider._id_from_url(task.url)
        title = grab.doc.select(AvitoXPathConstants.ADV_TITLE).text()
        price = AvitoUpdateSpider._price_from_string(grab.doc.select(AvitoXPathConstants.ADV_PRICE).text())

        description = grab.doc.select(AvitoXPathConstants.ADV_DESCRIPTION_TEXT)
        if not description:
            description = grab.doc.select(AvitoXPathConstants.ADV_DESCRIPTION_HTML)
        if not description:
            description = ""
        description_text = description.text()

        metadata_block = grab.doc.select(AvitoXPathConstants.ADV_METADATA_BLOCK)
        views = metadata_block.select(AvitoXPathConstants.ADV_METADATA_BLOCK_VIEWS)
        views_count = int(views.text().split("(", 1)[0].replace(" ", ""))

        metadata_id_date_text = metadata_block.select(AvitoXPathConstants.ADV_METADATA_BLOCK_DATE).text()
        date = AvitoUpdateSpider._date_from_meta_string(metadata_id_date_text)

        views_metadata = metadata_block.select(AvitoXPathConstants.ADV_METADATA_BLOCK_VIEWS_METADATA)
        if views_metadata:
            id_old = True
        else:
            id_old = False

        avito_adv = AvitoAdv(id)
        avito_adv.title = title
        avito_adv.url = task.url
        avito_adv.price = price
        avito_adv.description = description_text
        avito_adv.views_count = views_count
        avito_adv.placement_date = avito_adv.update_date = date
        avito_adv.last_checked = datetime.now();
        avito_adv.is_closed = False; #TODO Handle closed ADs
        avito_adv.is_old = id_old
        avito_adv.fk_city = AvitoUpdateSpider._city_from_url(avito_adv.url)
        avito_adv.fk_category = AvitoUpdateSpider._category_from_url(avito_adv.url)

        if self.args["is_debug"]:
            print("Object updated: ", avito_adv, "\n----------------")

        if self.args["adv_parsed_callback"]:
            self.args["adv_parsed_callback"](avito_adv)

        self.advs_processed += 1
        if self.args["adv_limit_count"] and self.args["adv_limit_count"] <= self.advs_processed:
            self.stop()

    def task_initial_fallback(self, task):
        if self.args["update_errors_file"]:
            file_name = self.args["update_errors_file"]
        else :
            file_name = "update_errors.log"

        with open(file_name, 'a') as file:
            file.write("[%s] %s\n" %(datetime.now(), task.url))

    @staticmethod
    def _date_from_string(date_string):
        date_string_lower = date_string.lower()
        date_components = date_string_lower.split(" ")
        assert 1 < len(date_components) < 4
        if len(date_components) == 2:
            if "сегодня" in date_string_lower:
                base_date = date.fromtimestamp(time.time())
            elif "вчера" in date_string_lower:
                base_date = date.fromtimestamp(time.time()) - timedelta(1)  # Today minus one day

            time_string = date_string_lower.split(" ", 1)[1]
            actual_date_time = datetime.strptime(time_string, "%H:%M")
            actual_date_time = actual_date_time.replace(base_date.year, base_date.month, base_date.day)
        else:
            try:
                actual_date_time = datetime.strptime(date_string_lower, "%d %B %H:%M")  # "4 Февраля 21:12"
                actual_date_time = actual_date_time.replace(year=datetime.now().year)

            except ValueError:
                actual_date_time = datetime.strptime(date_string_lower, "%d %B %Y")  # "20 ноября 2016"

        return actual_date_time

    @staticmethod
    def _date_from_meta_string(id_date_string):
        id_date_string_lower = id_date_string.lower()
        id_date_string_lower = id_date_string_lower.split(", размещено ")[1]
        id_date_string_lower = id_date_string_lower.replace(" в", "")
        return AvitoUpdateSpider._date_from_string(id_date_string_lower)

    @staticmethod
    def _price_from_string(price_string):
        price_string_lower = price_string.lower()
        if "₽" in price_string_lower:
            return price_string_lower.replace(" ", "").split("₽")[0];
        elif "руб." in price_string_lower:
            price_components = price_string_lower.replace(" ", "").split("руб.")[0];
            return int(price_components)
        elif "договорная" in price_string_lower:
            return -1
        return -2

    @staticmethod
    def _id_from_url(url):
        return url.split("_")[-1]

    @staticmethod
    def _city_from_url(url):
        assert "https://www.avito.ru/" in url
        return url.split("/")[3]

    def _category_from_url(url):
        assert "https://www.avito.ru/" in url
        url_components = url.split("/")
        return url[url.find(url_components[4]):url.find(url_components[-1]) - 1]

    def m_update_avito_adv(self, avito_adv, do_phone_request=False):
        assert avito_adv.url

        url = "https://m.avito.ru" + avito_adv.url
        self.grab.go(url)

        if self.isDebug:
            print("Updating item from URL: ", url, "...")


        description = self.grab.doc.select(AvitoXPathConstants.M_ADV_DESCRIPTION)
        if not description:
            description = ""
        avito_adv.description = description.text()

        metadata_views = self.grab.doc.select(AvitoXPathConstants.M_ADV_METADATA_VIEWS).text().lower()
        avito_adv.views_count = int(metadata_views.replace("просмотров: ", "").split(" ")[0])

        if do_phone_request:
            phone_url = self.grab.doc.rex_text(AvitoXPathConstants.M_ADV_PHONE_URL_REX)
            time.sleep(self.delay)
            self.grab.go(phone_url)
            try:
                phone_text = self.grab.doc.rex_text('tel:(.+)"').replace(" ", "").replace("-", "")
            except Exception:
                print("Error!!!!!!----------------")
                print(self.grab.doc.body.decode("utf-8"))
            avito_adv.phone = phone_text

        if self.isDebug:
            print("Object updated: ", avito_adv, "\n----------------")

        if self.item_parsed_callback:
            self.item_parsed_callback(avito_adv)

        time.sleep(self.delay)

    # get image url by pkey and ID
    @staticmethod
    def get_image_pkey(ad_id=False, ad_phone=False):
        if ad_id and ad_phone:
            ad_subhash = re.findall(r'[0-9a-f]+', ad_phone)
            if int(ad_id) % 2 == 0:
                ad_subhash.reverse()
            ad_subhash = ''.join(ad_subhash)
            return ad_subhash[::3]



if __name__ == '__main__':

    #Enabling Grab debug output
    logging.basicConfig(level=logging.DEBUG)
    datebase_manager = DBManager("datebase.db", base, is_debug=False)
    print("Datebase records before updating:-------------")
    datebase_manager.print_all_avito_advs()

    #Generate URLs of the search pages
    search_pages_urls = TaskManager.search_pages_urls(u"Arduino",u"", datebase_manager.city_with_name(City.NOVOSIBIRSK),[1])

    #Pull the URLs for every item
    search_spider = AvitoSearchSpider(thread_number=1, args={"is_debug": True})
    search_spider.initial_urls = search_pages_urls;
    start_time = time.time()
    search_spider.run()

    #Fetch an info from the ADs' pages
    update_spider = AvitoUpdateSpider(thread_number=1, args={"adv_parsed_callback": datebase_manager.synchronize_avito_adv,
                                                             "is_debug": True,
                                                             "adv_limit_count": 0})
    update_spider.initial_urls = search_spider.items_urls
    update_spider.run()

    print("Parsed in %s seconds -------------" % (time.time() - start_time))
    '''
    print("\nSearch Spider:")
    print(search_spider.render_stats())
    print("Update Spider:")
    print(update_spider.render_stats())
    '''
    print("\nMerged datebase:-------------")
    datebase_manager.print_all_avito_advs()